//
//  DSXMLParser.m
//  Calendar
//
//  Created by Dan Merlea on 04/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import "DSXMLParser.h"
#import "Song.h"

@interface DSXMLParser ()
{
    NSXMLParser *rssParser;
    NSMutableDictionary *item;
    NSMutableArray *songs;
    NSString *elementValue;
    Song *song;
}
@end

@implementation DSXMLParser


+ (DSXMLParser *)sharedInstance {
    static dispatch_once_t once;
    static DSXMLParser *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[DSXMLParser alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        songs = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)parseXMLAtUrl:(NSURL *)url {
    rssParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [rssParser setDelegate:self];
    [rssParser parse];
}

#pragma mark - NSXMLParser Delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"playoutItem"]) {
        song = [[Song alloc]init];
        song.title = attributeDict[@"title"];
        song.artist = attributeDict[@"artist"];
        song.album = attributeDict[@"album"];
        song.duration = attributeDict[@"duration"];
        song.coverImage = attributeDict[@"imageUrl"];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"playoutItem"]) {
        [songs addObject:song];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if ([self.delegate respondsToSelector:@selector(parseCompleteWithResult:)]) {
        [self.delegate parseCompleteWithResult:songs];
    }
}

@end
