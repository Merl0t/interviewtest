//
//  Utils.h
//  LikeOrDislike
//
//  Created by Dan Merlea on 07/12/14.
//
//

#import <UIKit/UIKit.h>

#define DEBUG                   1 // change to 0 on release

#if DEBUG == 0
#define DebugLog(...)
#elif DEBUG == 1
#define DebugLog(fmt,...)       NSLog((@"\n\n%s %s [Line %d]\n" fmt), __FILE__, __PRETTY_FUNCTION__, __LINE__, __VA_ARGS__)
#define DebugFrameLog(fmt,...)  NSLog(@"Frame: (%f,%f) (%f, %f)", fmt.frame.origin.x, fmt.frame.origin.y, fmt.frame.size.width, fmt.frame.size.height)
#endif

#define IMAGE_PLACEHOLDER       [UIImage imageNamed:@"placeholder.png"]
#define XML_URL                 @"http://apps.aim-data.com/data/abc/triplej/onair.xml"

#define CACHE_FOLDER            @"Images"

@interface Utils : NSObject

+ (NSString *)md5:(NSString *)input;

@end
