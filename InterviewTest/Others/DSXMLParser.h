//
//  DSXMLParser.h
//  Calendar
//
//  Created by Dan Merlea on 04/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DSXMLParserDelegate <NSObject>

- (void)parseCompleteWithResult:(NSMutableArray*)result;

@end

@interface DSXMLParser : NSObject <NSXMLParserDelegate>

@property (nonatomic, weak) id<DSXMLParserDelegate> delegate;

- (void)parseXMLAtUrl:(NSURL *)url;

@end
