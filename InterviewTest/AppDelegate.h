//
//  AppDelegate.h
//  InterviewTest
//
//  Created by Dan Merlea on 04/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

