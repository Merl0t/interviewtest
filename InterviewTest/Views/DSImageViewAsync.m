//
//  DSImageViewAsync.m
//  InterviewTest
//
//  Created by Dan Merlea on 04/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import "DSImageViewAsync.h"
#import "Utils.h"

@interface DSImageViewAsync ()
{
    UIImageView *imageView;
    UIActivityIndicatorView *loading;
    NSURLConnection *imageConnection;
    NSMutableData *activeDownload;
    NSString *hashedURL;
}
@end

@implementation DSImageViewAsync


-(id)initWithFrame:(CGRect)frame{
    if ((self = [super initWithFrame:frame])){
        [self initializeImage];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder])){
        [self initializeImage];
    }
    return self;
}

- (void)initializeImage {
    
    DebugFrameLog(self);
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [imageView setImage:IMAGE_PLACEHOLDER];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:imageView];
}

- (void)setImageURL:(NSString *)imageURL {
    
    _imageURL = imageURL;
    hashedURL = [Utils md5:self.imageURL];
    
    //fade in animation
    imageView.alpha = 0;
    
    DebugLog(@"HashURL: %@", hashedURL);
    
    if ([self isCached]) {
        imageView.image = [self loadImageFromCache];
        [self fadeImage];
        DebugLog(@"Cached!", nil);
        return;
    }
    
    if (imageConnection) {
        [self cancelDownload];
    }
    
    [self startDownload];
}

#pragma mark - Download Image

- (void)startDownload {
    
    [self showActivityIndicator];
    
    DebugLog(@"Start downloading image from %@", self.imageURL);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.imageURL]];
    imageConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)cancelDownload {
    
    [imageConnection cancel];
    
    activeDownload = nil;
    imageConnection = nil;
    
}

#pragma mark - Activity

- (void)showActivityIndicator {
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [loading setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [loading startAnimating];
    [self addSubview:loading];
}

- (void)hideActivityIndicator {
    [loading stopAnimating];
    [loading removeFromSuperview];
    loading = nil;
}

#pragma mark -

- (void)fadeImage {
    [UIView animateWithDuration:0.3 animations:^{
        imageView.alpha = 1;
    } completion:nil];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (activeDownload == nil) {
        activeDownload = [NSMutableData data];
    }
    [activeDownload appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    activeDownload = nil;
    imageConnection = nil;
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    UIImage *image = [[UIImage alloc] initWithData:activeDownload];
    imageView.image = image;
    [self fadeImage];
    
    activeDownload = nil;
    imageConnection = nil;
    
    [self hideActivityIndicator];
    [self cacheImage];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - Caching

- (BOOL)isCached {
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *cacheFolder = [cachesPath stringByAppendingPathComponent:@"Images"];
    NSString *tmpFile = [cacheFolder stringByAppendingPathComponent:hashedURL];
    return [[NSFileManager defaultManager] fileExistsAtPath:tmpFile];
}

- (UIImage*)loadImageFromCache {
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *cacheFolder = [cachesPath stringByAppendingPathComponent:@"Images"];
    NSString *tmpFile = [cacheFolder stringByAppendingPathComponent:hashedURL];
    return [UIImage imageWithContentsOfFile:tmpFile];
}

- (void)cacheImage {
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *cacheFolder = [cachesPath stringByAppendingPathComponent:@"Images"];
    NSError * error = nil;
    [[NSFileManager defaultManager] createDirectoryAtPath:cacheFolder
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                    error:&error];
    if (error != nil) {
        DebugLog(@"error creating directory: %@", error);
    }
    NSString *tmpFile = [cacheFolder stringByAppendingPathComponent:hashedURL];
    [UIImageJPEGRepresentation(imageView.image,1.0) writeToFile:tmpFile atomically:YES];
}


@end
