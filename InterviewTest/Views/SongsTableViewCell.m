//
//  SongsTableViewCell.m
//  InterviewTest
//
//  Created by Dan Merlea on 05/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import "SongsTableViewCell.h"

@implementation SongsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
