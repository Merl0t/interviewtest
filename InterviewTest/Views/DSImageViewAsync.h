//
//  DSImageViewAsync.h
//  InterviewTest
//
//  Created by Dan Merlea on 04/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface DSImageViewAsync : UIView <NSURLConnectionDelegate>

@property (nonatomic, strong) IBInspectable NSString *imageURL;

@end
