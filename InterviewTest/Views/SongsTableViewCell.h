//
//  SongsTableViewCell.h
//  InterviewTest
//
//  Created by Dan Merlea on 05/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSImageViewAsync.h"

@interface SongsTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *artistAndTitle;
@property (nonatomic, strong) IBOutlet UILabel *album;
@property (nonatomic, strong) IBOutlet UILabel *duration;
@property (nonatomic, strong) IBOutlet DSImageViewAsync *coverImage;

@end
