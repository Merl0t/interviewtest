//
//  ViewController.m
//  InterviewTest
//
//  Created by Dan Merlea on 04/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import "SongsTableViewController.h"
#import "StartViewController.h"
#import "DSImageViewAsync.h"
#import "DSXMLParser.h"
#import "Utils.h"

@interface StartViewController ()
{
    DSImageViewAsync *secondImageView;
}
@end

@implementation StartViewController

@synthesize firstImageView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    secondImageView = [[DSImageViewAsync alloc] initWithFrame:CGRectMake(10, 74, 140, 140)];
    [secondImageView setImageURL:@"http://thisisaim.com/wp-content/uploads/2014/09/aim_logo-800_flat.png"];
    [self.view addSubview:secondImageView];
    
    [secondImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    
    NSDictionary *views = NSDictionaryOfVariableBindings(secondImageView, firstImageView);
    
    [self.view addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"|-10.0-[secondImageView]-[firstImageView]"
                               options:NSLayoutFormatAlignAllTop
                               metrics:nil
                               views:views]];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"withLib"]) {
        SongsTableViewController *songTableViewController = (SongsTableViewController *)segue.destinationViewController;
        songTableViewController.withLibrary = YES;
    } else {
        SongsTableViewController *songTableViewController = (SongsTableViewController *)segue.destinationViewController;
        songTableViewController.withLibrary = NO;
    }
}

@end
