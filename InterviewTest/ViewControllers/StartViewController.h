//
//  ViewController.h
//  InterviewTest
//
//  Created by Dan Merlea on 04/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSImageViewAsync;

@interface StartViewController : UIViewController

@property (strong, nonatomic) IBOutlet DSImageViewAsync *firstImageView;

@end

