//
//  SongsTableViewController.h
//  InterviewTest
//
//  Created by Dan Merlea on 05/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSXMLParser.h"

@interface SongsTableViewController : UITableViewController <DSXMLParserDelegate>

@property (nonatomic) BOOL withLibrary;

@end
