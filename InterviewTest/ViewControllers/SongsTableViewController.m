//
//  SongsTableViewController.m
//  InterviewTest
//
//  Created by Dan Merlea on 05/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import "SongsTableViewController.h"
#import "SongsTableViewCell.h"
#import "XMLDictionary.h"
#import "Utils.h"
#import "Song.h"

@interface SongsTableViewController ()
{
    NSDictionary *songsData;
    NSMutableArray *songsArray;
    UIActivityIndicatorView *loading;
}
@end

@implementation SongsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.view.backgroundColor = [UIColor colorWithRed:0.135 green:0.534 blue:0.877 alpha:1.000];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loading setFrame:self.view.frame];
    [loading startAnimating];
    [self.view addSubview:loading];
    
    XMLDictionaryParser *xmlParser = [XMLDictionaryParser sharedInstance];
    
    NSURL *url = [NSURL URLWithString:XML_URL];
    
    if (self.withLibrary) {
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            songsData = [xmlParser dictionaryWithData:data];
            DebugLog(@"%@", songsData);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [loading removeFromSuperview];
            });
            
        }];
        [queue waitUntilAllOperationsAreFinished];
        
    } else {
        
        DSXMLParser *parser = [[DSXMLParser alloc] init];
        [parser setDelegate:self];
        [parser parseXMLAtUrl:url];
        
    }
    
}

#pragma mark - DSXMLParser Delegate

- (void)parseCompleteWithResult:(NSMutableArray *)result {
    songsArray = result;
    [self.tableView reloadData];
    [loading removeFromSuperview];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.withLibrary) {
        return [songsData[@"playoutData"][@"playoutItem"] count];
    } else {
        return [songsArray count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"Cell";
    
    SongsTableViewCell *cell = (SongsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[SongsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if (self.withLibrary) {
        NSArray *songs = songsData[@"playoutData"][@"playoutItem"];
        
        cell.artistAndTitle.text = [NSString stringWithFormat:@"%@ - %@", [songs objectAtIndex:indexPath.row][@"_artist"], [songs objectAtIndex:indexPath.row][@"_title"]];
        cell.album.text = [songs objectAtIndex:indexPath.row][@"_album"];
        cell.duration.text = [songs objectAtIndex:indexPath.row][@"_duration"];
        cell.coverImage.imageURL = [songs objectAtIndex:indexPath.row][@"_imageUrl"];
    } else {
        Song *song = [songsArray objectAtIndex:indexPath.row];
        
        cell.artistAndTitle.text = [NSString stringWithFormat:@"%@ - %@", song.artist, song.title];
        cell.album.text = song.album;
        cell.duration.text = song.duration;
        cell.coverImage.imageURL = song.coverImage;
    }

    
    return cell;
}

@end
