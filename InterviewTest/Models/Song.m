//
//  Song.m
//  InterviewTest
//
//  Created by Dan Merlea on 05/02/15.
//  Copyright (c) 2015 Dan Merlea. All rights reserved.
//

#import "Song.h"

@implementation Song

@synthesize artist, title, album, duration, coverImage;

- (NSString *)description {
    return [NSString stringWithFormat:@"artist = %@, title = %@, album = %@, duration = %@, coverImage = %@", artist, title, album, duration, coverImage];
}

@end
